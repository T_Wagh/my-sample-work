# My Sample Work

This repository contains 3 directories. Each directory has a code file and video demo.
All tasks have been implemented using a RaspberryPi 3B+.

## Camera

### Task 1: Python code to start Camera

Python code starts the camera. A web cam is connected via USB to te Raspberry Pi. Code uses the OpenCV library.

## Stepper Motor

### Task 2: Python code for speed control of Stepper motor

This setup uses a bipolar stepper motor, which is driven by a DRV 8825 Stepper motor driver.
Speed control achieved using bit banging method a well as pwm method (in two seperate code files)

## Sensors

### Task 3: Python code to interface sensors

Interfacing codes for the following sensors are included in this directory 

* MPU6050 - Acclerometer values obtained (uses I2C)
* HCSR-04 - Ultrasonic Sensor
