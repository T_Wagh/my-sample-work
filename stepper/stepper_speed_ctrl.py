import time
import RPi.GPIO as GPIO

STEP = 21  # Step  Pin

GPIO.setmode(GPIO.BCM)
GPIO.setup(STEP, GPIO.OUT)

step_count= 200*16 # 1/16 micro stepping

#speed 1
for i in range(step_count):
    GPIO.output(STEP, True)
    time. sleep(0.01)
    GPIO.output(STEP, False)
    time. sleep(0.01)

time.sleep(3)

#speed 2
for i in range(step_count):
    GPIO.output(STEP, True)
    time. sleep(0.005)
    GPIO.output(STEP, False)
    time. sleep(0.005)

time.sleep(3)

#speed 3
for i in range(step_count):
    GPIO.output(STEP, True)
    time. sleep(0.002)
    GPIO.output(STEP, False)
    time. sleep(0.002)

time.sleep(3)

GPIO.cleanup()
