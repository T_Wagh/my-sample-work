import RPi.GPIO as GPIO
import time
GPIO.setwarnings(False)

STEP=21
GPIO.setmode (GPIO.BCM)
GPIO.setup(STEP,GPIO.OUT)

#speed 1
pwm = GPIO.PWM(STEP, 100)         #100Hz frequency
pwm.start
time.sleep(15)
#speed 2
pwm.ChangeFrequency(500) 	  #500Hz frequency
time.sleep(15)
#speed3
pwm.ChangeFrequency(1000)	  #1000Hz frequency
pwm.stop()
GPIO.cleanup()
