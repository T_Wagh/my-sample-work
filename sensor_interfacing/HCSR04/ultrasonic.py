import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

trig = 23
echo = 24

GPIO.setup(trig, GPIO.OUT)
GPIO.setup(echo,GPIO.IN)
while 1:

    GPIO.output(trig, True)
    time.sleep(0.0001) # 10 micro sesonds pulse
    GPIO.output(trig, False)

    while GPIO.input(echo)==0: 
        start = time.time()
    while GPIO.input(echo)==1:
        end = time.time()
    t = end - start
    dist= round(t/0.000058, 2)
    print ('Distance: {} cm'.format (dist))
    time.sleep(0.5)
