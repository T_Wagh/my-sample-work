import time
import RPi.GPIO as gpio
import smbus


def read_I2Cdata(addr):
        acc_h = bus.read_byte_data(I2C_addr, addr)
        acc_l= bus.read_byte_data(I2C_addr, addr+1)
        value = ((acc_h << 8) | acc_l)
        if(value > 32768):
           value = value - 65536
        return value
        return value


bus = smbus.SMBus(1)

I2C_addr = 0x68   # MPU6050 device address
bus.write_byte_data(I2C_addr, 0x6B, 0) # power management reg
bus.write_byte_data(I2C_addr, 0x1C, 0b00011000) # set +/- 4g full scale reading

while True:

    #accelerometer raw values
    raw_x = read_I2Cdata(0x3B)
    raw_y = read_I2Cdata(0x3D)
    raw_z = read_I2Cdata(0x3F)

    # processed values (in gf)
    x = raw_x/2048.0
    y = raw_y/2048.0
    z = raw_z/2048.0

    print ("\tx=%.2f g" %x, "\ty=%.2f g" %y, "\tz=%.2f g" %z)
    time.sleep(0.5)


